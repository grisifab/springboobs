package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.CommentaireRepository;
import com.example.demo.models.Commentaire;

@RestController
public class CommentaireRestController {
	
	@Autowired
	private CommentaireRepository commentaireRepository;

	@GetMapping("/comms")
	@ResponseBody
	public String getComms() {
		return commentaireRepository.findAll().toString();
	}
	
	@GetMapping("/comms/{id}")
	@ResponseBody
	public String getComm(@PathVariable("id") long id) {
		return commentaireRepository.findById(id).orElse(null).toString();
	}
	
	@PostMapping("/comms")
	@ResponseBody
	public Commentaire addComm(@RequestBody Commentaire commentaire) {
		System.out.println(commentaire);
		return commentaireRepository.save(commentaire);
	}
	
	@PutMapping("/comms/{id}")
	public Commentaire updateComm(@PathVariable("id") long id, @RequestBody Commentaire commentaire) {
		commentaire.setId(id);
		return commentaireRepository.save(commentaire);
	}
	
	@DeleteMapping("/comms/{id}")
	public boolean deleteComm(@PathVariable("id") long id) {
		commentaireRepository.deleteById(id);
		return true;
	}
}
