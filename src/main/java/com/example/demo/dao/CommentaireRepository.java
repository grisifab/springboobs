package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.models.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Long> {

}
