package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringboobsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringboobsApplication.class, args);
	}

}
