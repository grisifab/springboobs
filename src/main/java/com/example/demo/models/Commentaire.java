package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Commentaire {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String libele;
	private String sujet;
	private String description;
	private String date;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibele() {
		return libele;
	}
	public void setLibele(String libele) {
		this.libele = libele;
	}
	public String getSujet() {
		return sujet;
	}
	public void setSujet(String sujet) {
		this.sujet = sujet;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Commentaire [id=" + id + ", libele=" + libele + ", sujet=" + sujet + ", description=" + description
				+ ", date=" + date + "]";
	}
	public Commentaire(String libele, String sujet, String description, String date) {
		super();
		this.libele = libele;
		this.sujet = sujet;
		this.description = description;
		this.date = date;
	}
	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
